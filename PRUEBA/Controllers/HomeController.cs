﻿using PRUEBA.Models;
using PRUEBA.services;
using PRUEBA.VIEWMODELS;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web.Mvc;


namespace PRUEBA.Controllers
{
    public class HomeController : Controller
    {
        private AgregarRespositories _repo;

        public HomeController()
        {
            _repo = new AgregarRespositories();
        }
        public ActionResult Index()
        {
            var model = _repo.ObtenerTodos();
            return View(model);

            ApplicationDbContext _context = new ApplicationDbContext();
            List<CLIENTE_VM> tmp = null;
            CLIENTELIST_VM VM = new CLIENTELIST_VM();
            try
            {
                tmp = _context.Database.SqlQuery<CLIENTE_VM>("EXEC SP_CLIENTE_SELECT").ToList();

            }
            catch (Exception ex)
            {
                tmp = new List<CLIENTE_VM>();
            }
            VM.CLIENTES = tmp;
            return View("Index", VM);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }


        public ActionResult Contact()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

     

        public ActionResult Details(int id)
        {
            return View();
        }

        public ActionResult Create(int id)
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(Agregar model)
        {
            try
            {
                if(ModelState.IsValid)
                {
                    _repo.Crear(model);
                    return RedirectToAction("Index");
                }                
            }
            catch(Exception ex)
            {
                // Log
            }
            return View(model);
        }

        


        public ActionResult addnew()
        {
            CLIENTE_VM model = new CLIENTE_VM();
            ApplicationDbContext _context = new ApplicationDbContext();
            List<TIPO_CLIENTEVM> TIPOS_CLIENTES = new List<TIPO_CLIENTEVM>();
            try
            {
                TIPOS_CLIENTES = _context.Database.SqlQuery<TIPO_CLIENTEVM>("EXEC SP_TIPOCLIENTES_SELECT").ToList();

            }
            catch (Exception ex)
            {
                TIPOS_CLIENTES = new List<TIPO_CLIENTEVM>();
            }
            TIPO_CLIENTEVM newitem = new TIPO_CLIENTEVM()

            {
                TIPOCLIEN_ID = -1,
                DESCRIPCION = "Seleccione...",
                STATUS = true
            };
            TIPOS_CLIENTES.Insert(0, newitem);

            model.TIPOS_CLIENTE = TIPOS_CLIENTES;


            return View("_addnew", model);
        }
        [HttpPost]


        public ActionResult saveNewitem(CLIENTE_VM model)
        {
            ApplicationDbContext _context = new ApplicationDbContext();
            string param = " @opc,@clienteid,@nombre,@tipo_cliente,@direccion,@status";
            var tmp
                = _context.Database.SqlQuery<object>("EXEC SP_CLIENTE_INSERT_UPDATE" + param,
                new SqlParameter("opc", 1),
                new SqlParameter("clienteid",model.CLIENTE_ID),
                new SqlParameter("nombre", model.NOMBRE),
                new SqlParameter("tipo_cliente", model.TIPO_CLIENTE),
                new SqlParameter("direccion", model.DIRECCION),
                new SqlParameter("status", model.STATUS)).ToList();

            List<CLIENTE_VM> tmp2 = null;
            CLIENTELIST_VM VM = new CLIENTELIST_VM();
            try
            {
                tmp2 = _context.Database.SqlQuery<CLIENTE_VM>("EXEC SP_CLIENTE_SELECT").ToList();

            }
            catch (Exception ex)
            {
                tmp2 = new List<CLIENTE_VM>();
            }
            VM.CLIENTES = tmp2;


            return View("_detalles", VM);
        }


        //public ActionResult updeta()
        //{
        //    CLIENTE_VM model = new CLIENTE_VM();
        //    ApplicationDbContext _context = new ApplicationDbContext();
        //    List<TIPO_CLIENTEVM> TIPOS_CLIENTES = new List<TIPO_CLIENTEVM>();
        //    try
        //    {
        //        TIPOS_CLIENTES = _context.Database.SqlQuery<TIPO_CLIENTEVM>("EXEC SP_TIPOCLIENTES_SELECT").ToList();

        //    }
        //    catch (Exception ex)
        //    {
        //        TIPOS_CLIENTES = new List<TIPO_CLIENTEVM>();
        //    }
        //    TIPO_CLIENTEVM newitem = new TIPO_CLIENTEVM()

        //    {
        //        TIPOCLIEN_ID = -1,
        //        DESCRIPCION = "Seleccione...",
        //        STATUS = true
        //    };
        //    TIPOS_CLIENTES.Insert(0, newitem);

        //    model.TIPOS_CLIENTE = TIPOS_CLIENTES;


        //    return View("_detalles", model);
        //}

        //public ActionResult saveNewitemm(CLIENTE_VM model)
        //{
        //    ApplicationDbContext _context = new ApplicationDbContext();
        //    string param = " @opc,@clienteid,@nombre,@tipo_cliente,@direccion,@status";
        //    var tmp
        //        = _context.Database.SqlQuery<object>("EXEC SP_CLIENTE_INSERT_UPDATE" + param,
        //        new SqlParameter("opc", 1),
        //        new SqlParameter("clienteid", model.CLIENTE_ID),
        //        new SqlParameter("nombre", model.NOMBRE),
        //        new SqlParameter("tipo_cliente", model.TIPO_CLIENTE),
        //        new SqlParameter("direccion", model.DIRECCION),
        //        new SqlParameter("status", model.STATUS)).ToList();

        //    List<CLIENTE_VM> tmp3 = null;
        //    CLIENTELIST_VM VM = new CLIENTELIST_VM();
        //    try
        //    {
        //        tmp3 = _context.Database.SqlQuery<CLIENTE_VM>("EXEC SP_CLIENTE_SELECT").ToList();

        //    }
        //    catch (Exception ex)
        //    {
        //        tmp3 = new List<CLIENTE_VM>();
        //    }
        //    VM.CLIENTES = tmp3;


        //    return View("_detalles", VM);
        //}
    }
}