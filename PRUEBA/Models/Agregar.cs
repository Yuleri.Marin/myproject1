﻿using System.Data.Entity;
namespace PRUEBA.Models
{
    public class Agregar
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public int Tipo_Cliente { get; set; }
        public string Direccion { get; set; }
        public bool Estado { get; set; }
}
}