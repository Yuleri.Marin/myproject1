﻿using System.Data.Entity;

namespace PRUEBA.Models
{
    public class AgregarContext : DbContext
    {
        public AgregarContext()
            :base("DefaultConnection")
            {

            }
        public DbSet<Agregar>Agregar{ get; set; }
    }
}