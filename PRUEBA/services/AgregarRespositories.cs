﻿using PRUEBA.Models;
using System.Collections.Generic;
using System.Linq;

namespace PRUEBA.services
{
    public class AgregarRespositories
    {
        public List<Agregar>ObtenerTodos()
        {
            using (var db = new AgregarContext())
            {
                return db.Agregar.ToList();
            }
        }

        internal void Crear(Agregar model)
        {
            using (var db = new AgregarContext())
            {
                db.Agregar.Add(model);
                db.SaveChanges();
            }
        }
    }
}