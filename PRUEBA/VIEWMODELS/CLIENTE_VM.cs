﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PRUEBA.VIEWMODELS
{
    public class CLIENTE_VM
    {
        public int CLIENTE_ID { get; set; }
        public string NOMBRE { get; set; }
        public int TIPO_CLIENTE { get; set; }
        public List <TIPO_CLIENTEVM> TIPOS_CLIENTE { get; set; }
        public string DIRECCION { get; set; }

        public bool STATUS { get; set; }
    

    }
    
}